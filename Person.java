/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zylkerrailways;

import java.util.ArrayList;

/**
 *
 * @author sundar-pt2602
 */public class Person {
    String name,prefer;
    int age,seatno;

    public Person() {
    }

    public Person(String name, String prefer, int age,int seatno) {
        this.name = name;
        this.prefer = prefer;
        this.age = age;
        this.seatno=seatno;
    }
    
    public void persondetail(ArrayList<Person> plist){
         System.out.format("%-10s %-10s %-5s %-10s ","seatno","name","age","prefer");
         System.out.println("");
        
      for(Person x:plist){
                System.out.format("%-10d %-10s %-5d %-10s ",x.seatno,x.name,x.age,x.prefer);
                System.out.println("");
            }
        
    }
}
