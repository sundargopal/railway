package zylkerrailways;

import java.util.*;

public class Compartment {

    Scanner sc = new Scanner(System.in);

    int seatno, age, pnr;
    String name, type, status = "AVL";
    int PNR = 457890;
    int low, up, mid, rac, wl;
    int wcount = 1;

    public Compartment() {
    }

    public void seatcal(int n) {
        this.low = 2 * n;
        this.up = 3 * n;
        this.mid = 2 * n;
        this.rac = 2 * n;
        this.wl = 2 * n;
    }
    
   

    public Compartment(int seatno, int age, int pnr, String name,String type) {
        this.seatno = seatno;
        this.age = age;
        this.name = name;

        this.pnr = pnr;
        if(seatno==0){
        this.type="WL";
        }
        else{
        int rem = seatno % 8;
        if (rem == 1 || rem == 4) {
            this.type = "Lower";
        }
        if (rem == 2 || rem == 5) {
            this.type = "Middle";
        }
        if (rem == 3 || rem == 6 || rem == 0) {
            this.type = "Upper";
        }
        if (rem == 7) {
            this.type = "RAC";
        }
        }
    }

    public void printticket(ArrayList<Compartment> clist) {

        for (Compartment x : clist) {
            System.out.format("%-10d %-5d %-10s %-10s %-10s %-5d", x.pnr, x.seatno, x.status, x.type, x.name, x.age);
            System.out.println("");
        }

    }

    public int avlnum(ArrayList<Compartment> clist) {
        int count = 0;
        for (Compartment x : clist) {
            if (x.status.equals("AVL")) {
                count++;
            }
        }

        return (count + wl);
    }

    public void ticketbooking(ArrayList<Compartment> clist, int num, ArrayList<Person> plist) {
        String prefer = "";
        PNR = PNR + 1;
        int child = 0;
        System.out.println("Are you have Child Please enter the child details at First  ");
        while (num > 0) {

            System.out.println("person Name");
            String name = sc.next();

            System.out.println("Person Age");
            boolean ex4 = true;
            int age = 0;
            do {
                sc.nextLine();
                try {
                    age = sc.nextInt();
                    ex4 = true;
                } catch (Exception e) {
                    ex4 = false;
                    System.out.println(" Enter age in Digits");
                }
            } while (!ex4);
            System.out.println("Birth Preferance  \n(Lower,Middle,Upper)");
            boolean ex5 = true;
            char pre = sc.next().toLowerCase().charAt(0);
            do {

                if (pre == 'l') {
                    prefer = "Lower";
                    ex5 = true;
                } else if (pre == 'u') {
                    prefer = "Upper";
                    ex5 = true;
                } else if (pre == 'm') {
                    prefer = "Middle";
                    ex5 = true;
                } else {
                    System.out.println("Enter lower (or)Upper (or) Middle");
                    ex5 = false;
                }
            } while (!ex5);

            if (age > 5) {
                if (child == 1) {
                    prefer = "Lower";
                    child = 2;
                }

                if (up <= 0 && (low > 0 && mid <= 0) || up <= 0 && (low > 0 && mid > 0)) {
                    prefer = "lower";
                }
                if (up <= 0 && (mid > 0 && low <= 0) || up <= 0 && (mid > 0 && low > 0)) {
                    prefer = "Middle";
                }
                if (low <= 0 && (mid > 0 && up <= 0) || low <= 0 && (mid > 0 && up > 0)) {
                    prefer = "Middle";
                }
                if (low <= 0 && (mid <= 0 && up > 0) || low <= 0 && (mid > 0 && up > 0)) {
                    prefer = "Upper";
                }
                if (mid <= 0 && (up > 0 && low <= 0) || mid <= 0 && (low > 0 && up > 0)) {
                    prefer = "Upper";
                }

                if (low <= 0 && up <= 0 && mid <= 0 && rac > 0) {
                    prefer = "RAC";
                }
                if (low <= 0 && up <= 0 && mid <= 0 && rac <= 0 && wl > 0) {
                    prefer = "WL";
                }

                if (prefer.equals("RAC")) {
                    for (Compartment x : clist) {
                        if (x.type.equalsIgnoreCase(prefer)) {
                            if (x.status.equals("AVL")) {
                                x.name = name;
                                x.age = age;
                                x.status = "CNF";
                                x.pnr = PNR;
                                rac--;
                                plist.add(new Person(x.name, x.type, x.age, x.seatno));
                                System.out.println("\n\nTicket Confirm " + "\n Your PNR is :" + x.pnr + "\n Name : " + x.name + "\nBirth Type :" + x.type);
                                break;
                            }
                        }
                    }
                } else if (prefer.equals("WL")) {
                     for (Compartment x : clist) {
                         if (x.type.equalsIgnoreCase(prefer)) {
                            if (x.status.equals("AVL")) {
                                x.name = name;
                                x.age = age;
                                x.status = "CNF";
                                x.pnr=0;
                                wl--;
                                plist.add(new Person(name, "WL", age, 0));
                              
                              System.out.println("Added successsfully In WaitingList");
                      break;
                     }
                   
                }
            }
                }
                if (low > 0) {
                    if (prefer.equalsIgnoreCase("lower")) {

                        for (Compartment x : clist) {
                            if (x.type.equalsIgnoreCase(prefer)) {
                                if (x.status.equals("AVL")) {
                                    x.name = name;
                                    x.age = age;
                                    x.status = "CNF";
                                    x.pnr = PNR;
                                    plist.add(new Person(x.name, x.type, x.age, x.seatno));
                                    System.out.println("\nTicket Confirm " + "\nYour PNR is :" + x.pnr + "\n Name : " + x.name + "\nBirth Type : " + x.type);
                                    low--;

                                    break;

                                }

                            }
                        }

                    }
                }
                if (mid > 0) {
                    if (prefer.equalsIgnoreCase("Middle")) {
                        int c = 0;
                        for (Compartment x : clist) {
                            if (x.type.equalsIgnoreCase(prefer)) {
                                if (x.status.equals("AVL")) {
                                    x.name = name;
                                    x.age = age;
                                    x.status = "CNF";
                                    x.pnr = PNR;
                                    System.out.println("\n\nTicket Confirm " + "\nYour PNR is :" + x.pnr + "\n Name : " + x.name + "\nBirth Type : " + x.type);
                                    plist.add(new Person(x.name, x.type, x.age, x.seatno));
                                    mid--;
                                    break;
                                }

                            }
                        }
                    }
                }
                if (up > 0) {
                    if (prefer.equalsIgnoreCase("Upper")) {
                        int c = 0;
                        for (Compartment x : clist) {
                            if (x.type.equalsIgnoreCase(prefer)) {
                                if (x.status.equals("AVL")) {
                                    x.name = name;
                                    x.age = age;
                                    x.status = "CNF";
                                    x.pnr = PNR;
                                    System.out.println("\n\nTicket Confirm " + "\nYour PNR is :" + x.pnr + "\n Name : " + x.name + "\nBirth Type : " + x.type);
                                    plist.add(new Person(x.name, x.type, x.age, x.seatno));
                                    up--;
                                    break;
                                }

                            }
                        }
                    }
                }
            }
            if (age <= 5) {
                plist.add(new Person(name, "", age, 0));
                child++;
            }
            num--;
        }
    }

    public void search(ArrayList<Compartment> clist) {
        System.out.println("Enter the PNR");
        boolean expnr=true;
        int num = 0;
        do{
             sc.nextLine();
             try{
                 System.out.println("Enter the PNR");
                 num = sc.nextInt();
                 expnr=true;
             }catch(Exception e){
                 expnr=false;
              }
        }while(!expnr);
        
        System.out.format("%-10s %-5s %-10s %-10s %-10s %-5s", "PNR", "Seatno", "Status", "Type", "Name", "Age");
        System.out.println("");

        for (Compartment x : clist) {
            if (x.pnr == num) {
                System.out.format("%-10d %-5d %-10s %-10s %-10s %-5d", x.pnr, x.seatno, x.status, x.type, x.name, x.age);
                System.out.println("");
            }
        }
    }

    public void cancel(ArrayList<Compartment> clist, ArrayList<Person> plist) {
        plist.clear();
        System.out.println("person Name");
        String name = sc.next();
        boolean expnr=true;
         int num=0;
         do{
             sc.nextLine();
             try{
                 System.out.println("Enter the PNR");
                 num = sc.nextInt();
                 expnr=true;
             }catch(Exception e){
                 expnr=false;
              }
        }while(!expnr);
         
         for (Compartment x : clist) {
            if (x.pnr == num && x.name.equals(name)) {
                x.name = " ";
                x.age = 0;
                x.status="AVL";
               
            }
         
         }
          
         ther: for (Compartment x : clist) {
            if (x.age == 0 && x.name.equals(" ")){
                  for (Compartment y : clist) {
                      if (y.type.equals("RAC")) {
                        x.name = y.name;
                        x.age = y.age;
                        y.name = " ";
                        y.age = 0;
                        break ther;
                    } 

                }
            }
        }
       
        int m=0,n;
         for (Compartment x : clist) {
             m++;
            if (x.age == 0 && x.name.equals(" ")) {
                n=0;
                for (Compartment y : clist) {
                    n++;
                    if(n>m){
                    if (y.type.equals("RAC")&&y.age!=0) {
                        x.name = y.name;
                        x.age =y.age;
                        y.name=" ";
                        y.age=0;
                        break ;
                    }
                 }
                }

            }
        }
       
        System.out.println("");
         here:     for (Compartment x : clist) {
            if (x.age == 0 && x.name.equals(" ")) {
                for (Compartment y : clist) {
                    if (y.type.equals("WL")) {
                        x.name = y.name;
                        x.age = y.age;
                        y.type = "RAC";
                        y.name = " ";
                        y.age = 0;
                        
                        break here;
                    }
                }
            }
        }
        System.out.println("");
        int k=0,l;
         for (Compartment x : clist) {
             k++;
            if (x.age == 0 && x.name.equals(" ")) {
                l=0;
                for (Compartment y : clist) {
                    l++;
                   if(l>k){
                    if (y.type.equals("WL")&&y.age!=0) {
                        x.name = y.name;
                        x.age = y.age;
                        y.name = " ";
                        y.age = 0;
                        y.type="WL";
                        break ;
                    }
                }
                }
            }
        }
      printticket(clist);
         
      for(Compartment x:clist){
           plist.add(new Person(x.name, x.type, x.age, x.seatno));
      }
    }
}
